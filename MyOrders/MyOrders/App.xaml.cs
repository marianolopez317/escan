﻿using MyOrders.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using System.Diagnostics;
using MyOrders.Helpers;

namespace MyOrders
{
    public partial class App : Application
    {
        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }

        public App()
        {
            InitializeComponent();
            // The root page of your application
            Debug.WriteLine("Se creo instancia de WelcomePage");
            if (Settings.GeneralSettings.Equals("1"))
            {
                MainPage= new MasterPage(); 
                
            }
            else
            {
                MainPage = new WelcomePage();
            }
            
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
