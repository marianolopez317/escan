// Helpers/Settings.cs
using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MyOrders.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

        

        #region Setting Constants

        private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

        
        private const string UserTypeKey = "usertype_key";
        private static readonly string UserTypeDefault = string.Empty;

        private const string UserIdKey = "userid_key";
        private static readonly string UserIdDefault = string.Empty;

        private const string UserCorreoKey = "usercorreo_key";
        private static readonly string UserCorreoDefault = string.Empty;

        private const string UserApPaternoKey = "userappaterno_key";
        private static readonly string UserApPaternoDefault = string.Empty;

        private const string UserApMaternoKey = "userapmaterno_key";
        private static readonly string UserApMaternoDefault = string.Empty;

        private const string UserNombreKey = "usernombre_key";
        private static readonly string UserNombreDefault = string.Empty;

        private const string UserSexoKey = "usersexo_key";
        private static readonly string UserSexoDefault = string.Empty;

        private const string UserFechaNacimientoKey = "userfechanacimiento_key";
        private static readonly string UserFechaNacimientoDefault = string.Empty;

        private const string UserContraseniaKey = "usercontrasenia_key";
        private static readonly string UserContraseniaDefault = string.Empty;


        private const string DogNombreKey = "dognombre_key";
        private static readonly string DogNombreDefault = string.Empty;


        private const string IdDogKey = "iddog_key";
        private static readonly string IdDogDefault = string.Empty;


        private const string DogOrigenKey = "dogorigen_key";
        private static readonly string DogOrigenDefault = string.Empty;

        private const string DogUsoKey = "doguso_key";
        private static readonly string DogUsoDefault = string.Empty;

        private const string DogEsperanzaVidaKey = "dogesperanzavida_key";
        private static readonly string DogEsperanzaVidaDefault = string.Empty;

        private const string AuxSexoKey = "auxsexo_key";
        private static readonly string AuxSexoDefault = string.Empty;
        #endregion


        public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}

        

        public static String UserType
        {
            get {
                return AppSettings.GetValueOrDefault(UserTypeKey, UserTypeDefault);
            }
            set {
                AppSettings.AddOrUpdateValue(UserTypeKey, value);
            }
        }

        public static String UserId
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserIdKey, UserIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserIdKey, value);
            }
        }

        public static String UserNombre
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserNombreKey, UserNombreDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserNombreKey, value);
            }
        }

        public static String UserApPaterno
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserApPaternoKey, UserApPaternoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserApPaternoKey, value);
            }
        }

        public static String UserApMaterno
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserApMaternoKey, UserApMaternoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserApMaternoKey, value);
            }
        }

        public static String UserCorreo
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserCorreoKey, UserCorreoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserCorreoKey, value);
            }
        }

        public static String UserSexo
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserSexoKey, UserSexoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserSexoKey, value);
            }
        }

        public static String UserFechaNacimiento
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserFechaNacimientoKey, UserFechaNacimientoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserFechaNacimientoKey, value);
            }
        }


        public static String UserContrasenia
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserContraseniaKey, UserContraseniaDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserContraseniaKey, value);
            }
        }


        public static String DogNombre
        {
            get
            {
                return AppSettings.GetValueOrDefault(DogNombreKey, DogNombreDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DogNombreKey, value);
            }
        }

        public static String IdDog
        {
            get
            {
                return AppSettings.GetValueOrDefault(IdDogKey, IdDogDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IdDogKey, value);
            }
        }

        public static String DogOrigen
        {
            get
            {
                return AppSettings.GetValueOrDefault(DogOrigenKey, DogOrigenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DogOrigenKey, value);
            }
        }

        public static String DogUso
        {
            get
            {
                return AppSettings.GetValueOrDefault(DogUsoKey, DogUsoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DogUsoKey, value);
            }
        }


        public static String DogEsperanzaVida
        {
            get
            {
                return AppSettings.GetValueOrDefault(DogEsperanzaVidaKey, DogEsperanzaVidaDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DogEsperanzaVidaKey, value);
            }
        }


        public static String AuxSexo
        {
            get
            {
                return AppSettings.GetValueOrDefault(AuxSexoKey, AuxSexoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(AuxSexoKey, value);
            }
        }

    }
}