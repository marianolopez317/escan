﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOrders.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyOrders.Pages
{
    
    public partial class RecuperarContraseniaPage : ContentPage
    {

        RecuperarContraseniaViewModel RecuperarContrasenia;
        public RecuperarContraseniaPage()
        {
            InitializeComponent();
            RecuperarContrasenia = new RecuperarContraseniaViewModel();
            BindingContext = RecuperarContrasenia;
        }
    }
}