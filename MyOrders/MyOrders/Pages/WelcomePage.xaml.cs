﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using MyOrders.ViewModels;
using System.Diagnostics;

namespace MyOrders.Pages
{
    
    public partial class WelcomePage : ContentPage
    {

        
        public WelcomeViewModel Welcome { get; private set; }
        public WelcomePage()
        {
            InitializeComponent();
            Welcome = new WelcomeViewModel();
            BindingContext = Welcome;
            //Debug.WriteLine("Se creo instancia de WelcomeViewModel llamado Welcome");
            
        }

        
    }
}
