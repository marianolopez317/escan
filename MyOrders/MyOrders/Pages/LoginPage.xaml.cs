﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MyOrders.ViewModels;

namespace MyOrders.Pages
{

    public partial class LoginPage : ContentPage
    {
        public LoginViewModel Login { get; private set; }
        public LoginPage ()
		{
            InitializeComponent();
            Login = new LoginViewModel();
            BindingContext = Login;

			
		}

        /*
        protected override bool OnBackButtonPressed()
        {
            // This prevents a user from being able to hit the back button and leave the login page.
            return true;
        }
        */
    }
}