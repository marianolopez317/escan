﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Helpers;
using MyOrders.Models;
using MyOrders.Services;
using MyOrders.ViewModels;
using Xamarin.Forms;

namespace MyOrders.Pages
{
    public partial class MyAccountPage : ContentPage
    {
        public MyAccountViewModel MyAccount { get; private set; }

        public MyAccountPage()
        {
            InitializeComponent();
            MyAccount = new MyAccountViewModel();
            BindingContext = MyAccount;
            if (Settings.AuxSexo.Equals("m"))
                pckSexo.SelectedIndex = 0;
            else if (Settings.AuxSexo.Equals("f"))
                pckSexo.SelectedIndex = 1;
        }

        private void pckSexo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var itemSelecionado = pckSexo.Items[pckSexo.SelectedIndex];
            if (itemSelecionado.Equals("Masculino"))
                Settings.AuxSexo = "m";
            else if (itemSelecionado.Equals("Femenino"))
                Settings.AuxSexo = "f";
        }



    }
}
