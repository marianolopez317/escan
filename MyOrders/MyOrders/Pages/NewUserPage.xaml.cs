﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;
using MyOrders.ViewModels;
using MyOrders.Helpers;

namespace MyOrders.Pages
{
    
    public partial class NewUserPage : ContentPage
    {
        public NewUserViewModel NewUser { get; private set; }
        public NewUserPage()
        {
            Debug.WriteLine("Se creo new user page");
            InitializeComponent();
            NewUser = new NewUserViewModel();
            BindingContext = NewUser;
        }

        private void pckSexo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var itemSelecionado = pckSexo.Items[pckSexo.SelectedIndex];
            if(itemSelecionado.Equals("Masculino"))
            Settings.AuxSexo = "m";
            else if (itemSelecionado.Equals("Femenino"))
                Settings.AuxSexo = "f";
        }




    }
}