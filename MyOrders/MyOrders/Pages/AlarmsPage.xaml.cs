﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyOrders.ViewModels;
using Xamarin.Forms;

namespace MyOrders.Pages
{
    public partial class AlarmsPage : ContentPage
    {

        public ClassifyViewModel Classify { get; private set; }
        public AlarmsPage()
        {
            InitializeComponent();
            Classify = new ClassifyViewModel();
            BindingContext= Classify;
            
        }

        
    }
}
