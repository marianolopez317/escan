﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOrders.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace MyOrders.Pages
{
    
    public partial class DogPage : ContentPage
    {
        public DogViewModel Dog { get; private set; }
        public DogPage()
        {
            InitializeComponent();
            Dog = new DogViewModel();
            BindingContext = Dog;
        }
    }
}