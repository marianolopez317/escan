﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Helpers;
using MyOrders.Models;
using Newtonsoft.Json;

namespace MyOrders.Services
{

    public class RazaService
    {
        DialogService dialogService;
        public RazaService()
        {
            dialogService = new DialogService();
        }

        public async Task<Raza> Get<Raza>(String id)
        {
            try
            {
                HttpClient client = new HttpClient();

                string url = "https://escan-181203.appspot.com/api/usuario/raza/" + id;

                var result = await client.GetAsync(url);

                string data = await result.Content.ReadAsStringAsync();

                if (result.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Raza>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(Raza);
        }

        public async Task<Resultado> Clasificar<Resultado>(String imagen)
        {
            //Debug.WriteLine("_______________________________imagen en base 64" + imagen);
            try
            {

                HttpClient client = new HttpClient();

                /*
                ImagePack pi = new ImagePack()
                {
                    IdUsuario=Settings.UserId,
                    Imagen=imagen
                };
                */

                //HttpContent content = new StringContent(JsonConvert.SerializeObject(pi),UnicodeEncoding.UTF8,"application/json");


                HttpContent content = new StringContent(
                    "imagen=" + System.Net.WebUtility.UrlEncode(imagen)
                );
                

                

                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                Debug.WriteLine("_______content" + content);
                string url = "https://escanescom.appspot.com/clasificar";
                var result = await client.PostAsync(url, content);


                string data = await result.Content.ReadAsStringAsync();
                Debug.WriteLine("_______data" + data);
                if (result.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Resultado>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(Resultado);
        }

        public async Task<MessageServer> GuardarHistorial<MessageServer>(String idUsuario, String idRaza)
        {
            try
            {
                Debug.WriteLine("guardar historial");
                Debug.WriteLine(idRaza+" "+idUsuario);
                HttpClient client = new HttpClient();



                HttpContent content = new StringContent(
                    "idUsuario=" + idUsuario + "&" +
                    "idRaza=" + idRaza
                );


                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                string url = "https://escan-181203.appspot.com/api/usuario/historial";
                var result = await client.PostAsync(url, content);


                string data = await result.Content.ReadAsStringAsync();
                Debug.WriteLine("data:"+data);
                if (result.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<MessageServer>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(MessageServer);
        }
    }
}
