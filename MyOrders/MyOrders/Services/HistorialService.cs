﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Helpers;
using MyOrders.Models;
using Newtonsoft.Json;

namespace MyOrders.Services
{
    public class HistorialService
    {
        DialogService dialogService;
        public HistorialService() {
            dialogService = new DialogService();
        }

        public async Task<List<Historial>> GetAllHistorial(String id)
        {
            try
            {
                HttpClient client = new HttpClient();

                string url = "https://escan-181203.appspot.com/api/usuario/historial/" + id;

                var result = await client.GetAsync(url);

                string data = await result.Content.ReadAsStringAsync();

                if (result.IsSuccessStatusCode)
                {

                    return JsonConvert.DeserializeObject<List<Historial>>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(List<Historial>);
        }

    }
}
