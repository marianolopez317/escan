﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Models;
using Newtonsoft.Json;

namespace MyOrders.Services
{
    public class CuidadoService
    {

        DialogService dialogService;

        public CuidadoService() {
            dialogService = new DialogService();
        }
        
        public async Task<List<Cuidado>> GetAllCuidados(String id)
        {
            try
            {
                HttpClient client = new HttpClient();

                
                string url = "https://escan-181203.appspot.com/api/usuario/cuidado/" + id;
                Debug.WriteLine("_____url " + url);
                var result = await client.GetAsync(url);

                string data = await result.Content.ReadAsStringAsync();
                Debug.WriteLine("_________" + data);
                if (result.IsSuccessStatusCode)
                {

                    return JsonConvert.DeserializeObject<List<Cuidado>>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(List<Cuidado>);
        }


        public async Task<List<Alimentacion>> GetAllAlimentacion(String id)
        {
            try
            {
                HttpClient client = new HttpClient();

                string url = "https://escan-181203.appspot.com/api/usuario/alimentacion/" + id;

                var result = await client.GetAsync(url);

                string data = await result.Content.ReadAsStringAsync();

                if (result.IsSuccessStatusCode)
                {

                    return JsonConvert.DeserializeObject<List<Alimentacion>>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(List<Alimentacion>);
        }


        public async Task<List<DatoCurioso>> GetAllDatosCuriosos(String id)
        {
            try
            {
                HttpClient client = new HttpClient();

                string url = "https://escan-181203.appspot.com/api/usuario/datoCurioso/" + id;

                var result = await client.GetAsync(url);

                string data = await result.Content.ReadAsStringAsync();

                if (result.IsSuccessStatusCode)
                {

                    return JsonConvert.DeserializeObject<List<DatoCurioso>>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(List<DatoCurioso>);
        }
    }
}
