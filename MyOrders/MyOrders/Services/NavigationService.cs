﻿using MyOrders.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using MyOrders.Helpers;
using MyOrders.Models;

namespace MyOrders.Services
{
    public class NavigationService
    {
        public async void Navigate(string pageName)
        {
            App.Master.IsPresented = false;
            switch (pageName)
            {
                
                
                case "AlarmsPage":
                    await Navigate(new AlarmsPage());
                    break;
                
                case "MyAccountPage":
                    UserService userService = new UserService();
                    User u = await userService.Get<User>(Settings.UserId);
                    if (u != null)
                    {
                        Debug.WriteLine(u.Nombre);
                        Settings.UserNombre = u.Nombre;
                        Debug.WriteLine(u.ApPaterno);
                        Settings.UserApPaterno = u.ApPaterno;
                        Debug.WriteLine(u.ApMaterno);
                        Settings.UserApMaterno = u.ApMaterno;
                        Debug.WriteLine(u.Sexo);
                        Settings.UserSexo = u.Sexo;
                        Settings.AuxSexo = u.Sexo;
                        Debug.WriteLine(u.FechaNacimiento);
                        Settings.UserFechaNacimiento = u.FechaNacimiento;
                        Debug.WriteLine(u.Correo);
                        Settings.UserCorreo = u.Correo;
                        Debug.WriteLine(u.Contrasenia);
                        Settings.UserContrasenia = u.Contrasenia;
                    }
                    await Navigate(new MyAccountPage());
                    break;

                case "NewOrderPage":

                    await Navigate(new NewOrderPage());
                    break;
                case "DogPage":

                    await Navigate(new DogPage());
                    break;


                case "MainPage":
                    await App.Navigator.PopToRootAsync();
                    break;

                case "WelcomePage":
                    Settings.GeneralSettings = "0";
                    Settings.UserId = null;
                    Settings.IdDog = null;
                    Settings.DogEsperanzaVida = null;
                    Settings.DogNombre= null;
                    Settings.DogOrigen = null;
                    Settings.DogUso = null;
                    
                    App.Current.MainPage = new WelcomePage();
                    break;
                default:
                    break;
            }
        }

        private static async Task Navigate<T>(T page) where T : Page
        {
            try
            {
                NavigationPage.SetHasBackButton(page, false);
                NavigationPage.SetBackButtonTitle(page, "Atrás"); //iOS
                await App.Navigator.PopToRootAsync();
                await App.Navigator.PushAsync(page);
                
            }
            catch (Exception e)
            {
                DialogService dialogService = new DialogService();
                await dialogService.ShowMessage(e.Message, "Error");
                
            }
        }

        

        

        internal void SetMainPage(string pageName)
        {
            switch (pageName)
            {
                case "MasterPage":
                    App.Current.MainPage = new MasterPage();
                    
                    break;
                case "MasterPage2":
                    App.Current.MainPage = new MasterPage();
                    
                    break;
                case "LoginPage":
                    App.Current.MainPage = new LoginPage();
                    
                    break;
                case "NewUserPage":
                    App.Current.MainPage = new NavigationPage(new NewUserPage());
                    
                    break;
                case "WelcomePage":
                    App.Current.MainPage = new WelcomePage();
                    
                    break;
                case "RecuperarContraseniaPage":
                    App.Current.MainPage = new RecuperarContraseniaPage();
                    break;
                default:
                    break;
            }
        }
    }
}
