﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Models;
using Newtonsoft.Json;

namespace MyOrders.Services
{
    public class UserService
    {

        DialogService dialogService = new DialogService();


        
        public async Task<MessageServer> PutUser<MessageServer>(User u, String id)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpContent content = new StringContent(
                    "correo=" + u.Correo + "&" +
                    "contrasenia=" + u.Contrasenia + "&" +
                    "nombre=" + u.Nombre + "&" +
                    "apPaterno=" + u.ApPaterno + "&" +
                    "apMaterno=" + u.ApMaterno + "&" +
                    "sexo=" + u.Sexo + "&" +
                    "fechaNacimiento=" + u.FechaNacimiento
                );
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                

                string url = "https://escan-181203.appspot.com/api/usuario/" + id;
                

                var result = await client.PutAsync(url,content);


                string data = await result.Content.ReadAsStringAsync();

                if (result.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<MessageServer>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(MessageServer);
        }

        public async Task<User> Get<User>(String id)
        {
            try {
                HttpClient client = new HttpClient();

                string url = "https://escan-181203.appspot.com/api/usuario/" + id;
                //client.DefaultRequestHeaders.Add("ZUMO-API-VERSION", "2.0.0");
                var result = await client.GetAsync(url);

                string data = await result.Content.ReadAsStringAsync();

                if (result.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<User>(data);
                }

            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(User);
        }

        public async Task<MessageServer> PostUser<MessageServer>(User u)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpContent content = new StringContent(
                    "correo=" + u.Correo + "&" + 
                    "contrasenia=" + u.Contrasenia + "&" +
                    "nombre=" + u.Nombre + "&" +
                    "apPaterno=" + u.ApPaterno + "&" +
                    "apMaterno=" + u.ApMaterno + "&" +
                    "sexo=" + u.Sexo + "&" +
                    "fechaNacimiento=" + u.FechaNacimiento
                );
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                var response = await client.PostAsync("https://escan-181203.appspot.com/api/usuario", content);
                if (response.IsSuccessStatusCode)
                {
                    string data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MessageServer>(data);
                }
            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(MessageServer);

        }

        public async Task<int> CreateUser(User u)
        {
            MessageServer m = await PostUser<MessageServer>(u);
            return m.Status;
        }


        public async Task<int> UpdateUser(User u, String id)
        {
            MessageServer m = await PutUser<MessageServer>(u,id);
            return m.Status;
        }
    }
}
