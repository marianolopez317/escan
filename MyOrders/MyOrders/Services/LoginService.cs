﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Helpers;
using MyOrders.Models;
using Newtonsoft.Json;

namespace MyOrders.Services
{
    public class LoginService
    {
        DialogService dialogService;
        public LoginService() {
            dialogService = new DialogService();
        }

        

        public async Task<MessageServer> CheckInServer<MessageServer>(String correo, String contrasenia) {
            try
            {
                HttpClient client = new HttpClient();
                HttpContent content = new StringContent("correo=" + correo + "&" + "contrasenia=" + contrasenia);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                var response = await client.PostAsync("https://escan-181203.appspot.com/api/usuario/login", content);
                if (response.IsSuccessStatusCode)
                {
                    string data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MessageServer>(data);
                }
            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(MessageServer);

        }


        public async Task<MessageServer> ObtenerContrasenia<MessageServer>(String correo)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpContent content = new StringContent("correo=" + correo);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                var response = await client.PostAsync("https://escan-181203.appspot.com/api/usuario/recuperarContrasenia", content);
                if (response.IsSuccessStatusCode)
                {
                    string data = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MessageServer>(data);
                }
            }
            catch (Exception e)
            {
                await dialogService.ShowMessage("Se requiere conexión a internet", "Error de conexión");
            }

            return default(MessageServer);

        }
        public async Task<int> IniciarSesion(Login l)
        {
            MessageServer m = await CheckInServer<MessageServer>(l.CorreoElectronico, l.Contrasenia);
            if (m.Status == 1)
                Settings.UserId = m.Mensaje;
            return m.Status;
            

        }

        public async Task<MessageServer> RecuperarContrasenia(String correo)
        {
            MessageServer m = await ObtenerContrasenia<MessageServer>(correo);
            
                
            return m;


        }
    }
}
