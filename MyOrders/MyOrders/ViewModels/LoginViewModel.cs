﻿using GalaSoft.MvvmLight.Command;
using MyOrders.Models;
using MyOrders.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Diagnostics;
using Xamarin.Forms;
using MyOrders.Helpers;

namespace MyOrders.ViewModels
{
    public class LoginViewModel:Login
    {
        NavigationService navigationService;
        DialogService dialogService;
        Login login;
        LoginService loginServicio;

        public LoginViewModel() {
            dialogService = new DialogService();
            navigationService = new NavigationService();
            IniciarSesionCommand = new Command(async () => await IniciarSesion(), () => !IsBusy);
            RecuperarContraseniaCommand = new Command(async () => await RecuperarContrasenia(), () => !IsBusy);
        }

        public Command RecuperarContraseniaCommand { set; get; }

        public Command IniciarSesionCommand { set; get; }
        private async Task RecuperarContrasenia()
        {
            navigationService.SetMainPage("RecuperarContraseniaPage");
        }
            private async Task IniciarSesion()
        {
            IsBusy = true;
            if (CorreoElectronico != null)
            {
                if (Contrasenia != null)
                {
                    login = new Login()
                    {
                        CorreoElectronico = CorreoElectronico,
                        Contrasenia = Contrasenia

                    };
                    loginServicio = new LoginService();
                    int r = await loginServicio.IniciarSesion(login);
                        
                    if (r==1)
                    {    
                        Settings.GeneralSettings = "1";
                        Settings.UserType = "1";    
                        navigationService.SetMainPage("MasterPage");
                           
                    }
                    else
                    {
                        MensajeInicioSesion = "Usuario o contraseña incorrecta";
                            
                            
                    }
                    IsBusy = false;
                }
                else
                {
                    MensajeInicioSesion = "La contraseña es requerida";
                    IsBusy = false;
                }

            }
            else
            {
                MensajeInicioSesion = "El email es requerido";   
                IsBusy = false;
            }
            
            
        }
    }
}
