﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Helpers;
using MyOrders.Models;
using MyOrders.Services;
using Xamarin.Forms;

namespace MyOrders.ViewModels
{
    public class DogViewModel:Dog
    {
        CuidadoService cuidadoService;
        NavigationService navigationService;
        DialogService dialogService;
        public ObservableCollection<CuidadoViewModel> Cuidados { get; set; }
        public ObservableCollection<CuidadoViewModel> Alimentacions { get; set; }
        public ObservableCollection<CuidadoViewModel> DatoCuriosos { get; set; }
        


        public DogViewModel() {
            
            
            LoadData(Settings.DogNombre);
            navigationService = new NavigationService();
            dialogService = new DialogService();
            cuidadoService = new CuidadoService();
            Cuidados = new ObservableCollection<CuidadoViewModel>();
            Alimentacions = new ObservableCollection<CuidadoViewModel>();
            DatoCuriosos = new ObservableCollection<CuidadoViewModel>();
            CuidadoCommand = new Command(async () => await Cuidado(), () => !IsBusy);
            AlimentacionCommand = new Command(async () => await Alimentacion(), () => !IsBusy);
            DatosCuriososCommand = new Command(async () => await DatosCuriosos(), () => !IsBusy);
            
        }

        public Command CuidadoCommand { set; get; }
        public Command AlimentacionCommand { set; get; }
        public Command DatosCuriososCommand { set; get; }
        private void LoadData(String name) {

            switch (name)
            {
                case "Bulldog":
                    Imagen = "bulldog.png";

                    break;
                case "Boxer":
                    Imagen = "Boxer.png";

                    break;
                case "Bull Terrier":
                    Imagen = "bullterrier.jpg";

                    break;
                case "Chihuahua":
                    Imagen = "chihuahua.png";

                    break;
                case "Pastor Aleman":
                    Imagen = "pastor_aleman.jpg";

                    break;
                case "Pitbull":
                    Imagen = "pitbull.png";

                    break;
                case "Pug":
                    Imagen = "pug.jpg";

                    break;
                case "Schnauzer":
                    Imagen = "schnauzer.png";

                    break;
                case "Yorkshire":
                    Imagen = "yor.jpg";

                    break;
                case "Labrador":
                    Imagen = "labrador.png";

                    break;



            }
            Nombre = Settings.DogNombre;
            Origen = "Origen: "+Settings.DogOrigen;
            Uso = "Uso: "+Settings.DogUso;
            EsperanzaVida = "Esperanza de vida:"+Settings.DogEsperanzaVida;
            
            
        }

        private async Task Cuidado()
        {
            
            IsBusy = true;
            
            Cuidados.Clear();
                
            var list = await cuidadoService.GetAllCuidados(Settings.IdDog);
            if (list.Count > 0)
            {
                foreach (var item in list)
                {

                    if (item.Higiene != null)
                    {
                        Cuidados.Add(new CuidadoViewModel()
                        {
                            Titulo = "Higiene",
                            Descripcion = item.Higiene
                        });
                    }
                    if (item.Pelo != null)
                    {
                        Cuidados.Add(new CuidadoViewModel()
                        {
                            Titulo = "Pelo",
                            Descripcion = item.Pelo
                        });
                    }
                    if (item.Enfermedad != null)
                    {
                        Cuidados.Add(new CuidadoViewModel()
                        {
                            Titulo = "Enfermedad",
                            Descripcion = item.Enfermedad
                        });
                    }
                    if (item.Vacuna != null)
                    {
                        Cuidados.Add(new CuidadoViewModel()
                        {
                            Titulo = "Vacuna",
                            Descripcion = item.Vacuna
                        });
                    }


                }
            }
            else
            {
                Cuidados.Add(new CuidadoViewModel()
                {
                    Titulo = "Sin información",
                    Descripcion = null
                });
            }
            IsDatosCuriosos = false;
            IsAlimentacion = false;
            IsCuidado = true;
            IsBusy = false;
        }

        private async Task Alimentacion()
        {


            IsBusy = true;
            

            Alimentacions.Clear();

            var list = await cuidadoService.GetAllAlimentacion(Settings.IdDog);
            if (list.Count > 0)
            {
                foreach (var item in list)
                {

                    if (item.Tipo != null)
                    {
                        Alimentacions.Add(new CuidadoViewModel()
                        {
                            Titulo = "Tipo",
                            Descripcion = item.Tipo
                        });
                    }
                    if (item.Porcion != null)
                    {
                        Alimentacions.Add(new CuidadoViewModel()
                        {
                            Titulo = "Porcion",
                            Descripcion = item.Porcion
                        });
                    }
                    if (item.Frecuencia != null)
                    {
                        Alimentacions.Add(new CuidadoViewModel()
                        {
                            Titulo = "Frecuencia",
                            Descripcion = item.Frecuencia
                        });
                    }



                }
            }
            else
            {
                Alimentacions.Add(new CuidadoViewModel()
                {
                    Titulo = "Sin información",
                    Descripcion = null
                });
            }
            IsDatosCuriosos = false;
            IsAlimentacion = true;
            IsCuidado = false;
            IsBusy = false;
            
        }

        private async Task DatosCuriosos()
        {

            IsBusy = true;
            

            DatoCuriosos.Clear();

            var list = await cuidadoService.GetAllDatosCuriosos(Settings.IdDog);
            if (list.Count > 0) {
                foreach (var item in list)
                {

                    if (item.Dato != null)
                    {
                        DatoCuriosos.Add(new CuidadoViewModel()
                        {
                            Titulo = "¿Sabías que?",
                            Descripcion = item.Dato
                        });
                    }

                }
            }else
            {
                DatoCuriosos.Add(new CuidadoViewModel()
                {
                    Titulo = "Sin información",
                    Descripcion = null
                });
            }

            IsDatosCuriosos = true;
            IsAlimentacion = false;
            IsCuidado = false;
            IsBusy = false;
            
        }

    }
}
