﻿using System;
using System.IO;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;
using MyOrders.Helpers;
using MyOrders.Models;
using MyOrders.Services;
using Plugin.Media;
using Xamarin.Forms;
using Plugin.ImageEdit;
using System.Diagnostics;
using Plugin.Geolocator;

namespace MyOrders.ViewModels
{

    public class ClassifyViewModel:Foto
    {
        NavigationService navigationService;
        DialogService dialogService;
        RazaService razaService;



        public ClassifyViewModel() {
            navigationService = new NavigationService();
            razaService = new RazaService();
            dialogService = new DialogService();
            TomarFotoCommand = new Command(async () => await TomarFoto(), () => !IsBusy);
            ElegirImagenCommand = new Command(async () => await ElegirImagen(), () => !IsBusy);
            ClasificarCommand = new Command(async () => await Clasificar(), () => !IsBusy);

        }
        public Command TomarFotoCommand { set; get; }
        public Command ElegirImagenCommand { set; get; }
        public Command ClasificarCommand { set; get; }

        

        
        private async Task Clasificar()
        {
            Img = null;
            IsBusy = true;
            IsTaken = false;
            try
            {

                
                
                Resultado rest = await razaService.Clasificar<Resultado>(Base64);
                
                if (rest.IdRaza.Equals("0"))
                {
                    await dialogService.ShowMessage("Problemas con la conexión a internet", "Error");
                
                }
                else {
                    MessageServer m = await razaService.GuardarHistorial<MessageServer>(Settings.UserId, rest.IdRaza);
                    Settings.IdDog = rest.IdRaza;
                    Raza r = await razaService.Get<Raza>(Settings.IdDog);
                    

                    if (r != null)
                    {
                        Settings.DogNombre = r.Nombre;
                        Settings.DogUso = r.Uso;
                        Settings.DogOrigen = r.Origen;
                        Settings.DogEsperanzaVida = r.EsperanzaVida;
                        IsBusy = false;
                        navigationService.Navigate("DogPage");
                    }
                }
                
                
            }
            catch(Exception e)
            {
                await dialogService.ShowMessage(e.Message, "Error");
            }
            
            
            
            
            IsBusy = false;


            
            
        }




        

        private async Task ElegirImagen()
        {
            IsTaken = false;
            Img = null;
            IsBusy = true;
            if (CrossMedia.Current.IsTakePhotoSupported)
            {
                var media = await CrossMedia.Current.PickPhotoAsync();
                if (media != null)
                {
                    try
                    {
                        var stream = media.GetStream();
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        stream.Seek(0, SeekOrigin.Begin);
                        byte[] ImageBytes = buffer;
                        media.Dispose();
                        var image = await CrossImageEdit.Current.CreateImageAsync(ImageBytes);
                        image.Resize(480, 480);
                        var jpgBytes = image.ToJpeg(100);
                        Base64 = Convert.ToBase64String(jpgBytes);
                        Img = ImageSource.FromStream(() => new MemoryStream(jpgBytes));
                        IsTaken = true;
                        
                    }
                    catch(Exception e) {
                        await dialogService.ShowMessage("La imagen no es compatible", "Error");
                    }

                }
            }
            IsBusy = false;
            //await Task.Delay(2000);


        }

        private async Task TomarFoto()
        {
            await dialogService.ShowMessage("La cara del perro debe ser complemente visible y no debe de estar a más de 1 metro de distancia", "Aviso");
            IsTaken = false;
            Img = null;
            IsBusy = true;

            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;

            var position = await locator.GetPositionAsync(10000);

            Debug.WriteLine("Position Status: {0}", position.Timestamp);
            Debug.WriteLine("Position Latitude: {0}", position.Latitude);
            Debug.WriteLine("Position Longitude: {0}", position.Longitude);


            var opciones_almacenamiento = new StoreCameraMediaOptions()
            {
                Name = "MiPerro.jpg",SaveToAlbum=true,Directory="ESCAN"

            };
            var media = await CrossMedia.Current.TakePhotoAsync(opciones_almacenamiento);

            if (media != null)
            {

                try
                {
                    var stream = media.GetStream();
                    var buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    stream.Seek(0, SeekOrigin.Begin);
                    byte[] ImageBytes = buffer;
                    media.Dispose();
                    var image = await CrossImageEdit.Current.CreateImageAsync(ImageBytes);
                    image.Resize(480, 480);
                    var jpgBytes = image.ToJpeg(100);
                    Base64 = Convert.ToBase64String(jpgBytes);
                    Img = ImageSource.FromStream(() => new MemoryStream(jpgBytes));
                    IsTaken = true;
                    
                }
                catch (Exception e)
                {
                    await dialogService.ShowMessage("La camara no es compatible", "Error");
                }

            }
            IsBusy = false;
        }
        

    }
}
