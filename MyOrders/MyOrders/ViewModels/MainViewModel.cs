﻿using GalaSoft.MvvmLight.Command;
using MyOrders.Pages;
using MyOrders.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Diagnostics;
using MyOrders.Helpers;
using MyOrders.Models;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MyOrders.ViewModels
{
    public class MainViewModel: IsBusyMainPage
    {

        NavigationService navigationService;
        HistorialService historialService;
        public OrderViewModel NewOrder { get; private set; }
        public ObservableCollection<OrderViewModel> Orders { get; set; }
        public ObservableCollection<MenuItemViewModel> Menu { get; set; }




        public MainViewModel()
        {
            navigationService = new NavigationService();
            historialService = new HistorialService();
            RefreshCommand = new Command(async () => await LoadPull(), () => !IsBusy);
            LoadMenu();
            Orders = new ObservableCollection<OrderViewModel>();
            LoadDataAsync();
        }
        
            
        public Command RefreshCommand { set; get; }

        public ICommand GoToCommand
        {
            get { return new RelayCommand<string>(GoTo); }
        }

        private void GoTo(string pageName)
        {
            navigationService.Navigate(pageName);
        }
        


       


        #region Methods
        private void LoadMenu()
        {
            Menu = new ObservableCollection<MenuItemViewModel>();

            Menu.Add(new MenuItemViewModel()
            {
                Icon = "ic_action_orders",
                Title = "Mi contenido",
                PageName = "MainPage"
            });

            Menu.Add(new MenuItemViewModel()
            {
                Icon = "ic_action_camara",
                Title = "Clasificar",
                PageName = "AlarmsPage"
            });



            Menu.Add(new MenuItemViewModel()
            {
                Icon = "ic_action_account",
                Title = "Mi Perfil",
                PageName = "MyAccountPage"
            });

            Menu.Add(new MenuItemViewModel()
            {
                Icon = "ic_action_shut_down",
                Title = "Cerrar sesión",
                PageName = "WelcomePage"
            });
        }

        private async Task LoadDataAsync()
        {
                
            
            Orders.Clear();
            if (Settings.UserId != null)
            {
                
                var list = await historialService.GetAllHistorial(Settings.UserId);
                if (list.Count > 0) { 
                    foreach (var item in list)
                    {
                        if (item.Fecha != null)
                        {
                            String f = item.Fecha;
                            f = f.Substring(0, 10);
                            switch (item.IdRaza)
                            {
                                case 1:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "Boxer.png",
                                        Title = "Boxer",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "1"
                                    });
                                    break;
                                case 2:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "bulldog.png",
                                        Title = "Bulldog",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "2"
                                    });
                                    break;
                                case 3:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "bullterrier.jpg",
                                        Title = "Bull Terrier",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "3"
                                    });
                                    break;
                                case 4:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "chihuahua.png",
                                        Title = "Chihuahua",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "4"
                                    });
                                    break;
                                case 5:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "pastor_aleman.jpg",
                                        Title = "Pastor Aleman",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "5"
                                    });
                                    break;
                                case 6:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "pitbull.png",
                                        Title = "Pitbull",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "6"
                                    });
                                    break;
                                case 7:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "pug.jpg",
                                        Title = "Pug",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "7"
                                    });
                                    break;
                                case 8:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "schnauzer.png",
                                        Title = "Schnauzer",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "8"

                                    });
                                    break;
                                case 9:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "yor.jpg",
                                        Title = "Yorkshire",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "9"

                                    });
                                    break;
                                case 10:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "labrador.png",
                                        Title = "Labrador",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "10"

                                    });
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    Orders.Add(new OrderViewModel()
                    {
                        Icon = "ic_triste.png",
                        Title = "Sin contenido ",
                        Fecha = null,
                        Description = null,
                        Id = null

                    });

                }


            }
            else
            {
                Orders.Add(new OrderViewModel()
                {
                    Icon = "ic_triste.png",
                    Title = "Jala para cargar tu contenido ",
                    Fecha = null,
                    Description = null,
                    Id = null

                });
            }
        }


        private async Task LoadPull()
        {
            IsBusy = true;

            Orders.Clear();
            if (Settings.UserId != null)
            {
                
                var list = await historialService.GetAllHistorial(Settings.UserId);
                if (list.Count > 0) { 
                    foreach (var item in list)
                    {
                        if (item.Fecha != null)
                        {
                            String f = item.Fecha;
                            f = f.Substring(0, 10);
                            switch (item.IdRaza)
                            {
                                case 1:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "Boxer.png",
                                        Title = "Boxer",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "1"
                                    });
                                    break;
                                case 2:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "bulldog.png",
                                        Title = "Bulldog",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "2"
                                    });
                                    break;
                                case 3:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "bullterrier.jpg",
                                        Title = "Bull Terrier",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "3"
                                    });
                                    break;
                                case 4:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "chihuahua.jpg",
                                        Title = "Chihuahua",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "4"
                                    });
                                    break;
                                case 5:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "pastor_aleman.jpg",
                                        Title = "Pastor Aleman",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "5"
                                    });
                                    break;
                                case 6:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "pitbull.png",
                                        Title = "Pitbull",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "6"
                                    });
                                    break;
                                case 7:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "pug.jpg",
                                        Title = "Pug",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "7"
                                    });
                                    break;
                                case 8:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "schnauzer.png",
                                        Title = "Schnauzer",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "8"

                                    });
                                    break;
                                case 9:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "yor.jpg",
                                        Title = "Yorkshire",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "9"

                                    });
                                    break;
                                case 10:
                                    Orders.Add(new OrderViewModel()
                                    {
                                        Icon = "labrador.png",
                                        Title = "Labrador",
                                        Fecha = f,
                                        Description = "Clasificado",
                                        Id = "10"

                                    });
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    Orders.Add(new OrderViewModel()
                    {
                        Icon = "ic_triste.png",
                        Title = "Sin contenido ",
                        Fecha = null,
                        Description = null,
                        Id = null

                    });

                }
            }
            else
            {
                Orders.Add(new OrderViewModel()
                {
                    Icon = "ic_triste.png",
                    Title = "Jala para cargar tu contenido ",
                    Fecha = null,
                    Description = null,
                    Id = null

                });
            }

            IsBusy = false;

        }
        #endregion

    }

}
