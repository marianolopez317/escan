﻿using GalaSoft.MvvmLight.Command;
using MyOrders.Models;
using MyOrders.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Diagnostics;
using MyOrders.Helpers;
using Xamarin.Forms;

namespace MyOrders.ViewModels
{
    public class OrderViewModel
    {
        
        DialogService dialogService;
        NavigationService navigationService;
        RazaService razaService;
        public OrderViewModel()
        {
            razaService = new RazaService();
            
            dialogService = new DialogService();
            navigationService = new NavigationService();
            
            SelectDogCommand = new Command(async () => await GoToDog());
        }

        public string Icon { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Fecha { get; set; }


        public Command SelectDogCommand { set; get; }
        

        private async Task GoToDog()
        {
            if (Id != null) {
                Settings.DogNombre = Title;
                Settings.IdDog = Id;
                Raza r = await razaService.Get<Raza>(Settings.IdDog);
                if (r != null)
                {
                    Settings.DogNombre = r.Nombre;
                    Settings.DogUso = r.Uso;
                    Settings.DogOrigen = r.Origen;
                    Settings.DogEsperanzaVida = r.EsperanzaVida;
                }
                navigationService.Navigate("DogPage");
            }
            
        }










    }
}
