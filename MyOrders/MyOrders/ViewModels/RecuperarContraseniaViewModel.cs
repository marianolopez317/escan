﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOrders.Models;
using MyOrders.Services;
using Xamarin.Forms;

namespace MyOrders.ViewModels
{
    public class RecuperarContraseniaViewModel : Login
    {
        NavigationService navigationService;
        DialogService dialogService;
        LoginService loginServicio;

        public RecuperarContraseniaViewModel()
        {
            dialogService = new DialogService();
            navigationService = new NavigationService();
            RecuperarCommand = new Command(async () => await Recuperar(), () => !IsBusy);
            HomeCommand = new Command(async () => await Home(), () => !IsBusy);

        }
        public Command RecuperarCommand { set; get; }
        public Command HomeCommand { set; get; }


        private async Task Recuperar()
        {
            IsBusy = true;
            if (CorreoElectronico != null)
            {
                loginServicio = new LoginService();
                MessageServer m = await loginServicio.RecuperarContrasenia(CorreoElectronico);
                if (m.Status == 1) {
                    IsBusy = false;
                    await dialogService.ShowMessage("Tu contraseña es: "+m.Mensaje, "Información");
                }
                else
                {
                    IsBusy = false;
                    await dialogService.ShowMessage("El email no esta registrado", "Error");
                }
            }
            else {
                IsBusy = false;
                await dialogService.ShowMessage("Ingresa tu email", "Error");
            }
            
        }

        private async Task Home()
        {
            navigationService.SetMainPage("LoginPage");
        }


    }   
}
