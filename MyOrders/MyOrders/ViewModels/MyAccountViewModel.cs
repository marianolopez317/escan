﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using MyOrders.Helpers;
using MyOrders.Models;
using MyOrders.Services;

namespace MyOrders.ViewModels
{
    public class MyAccountViewModel:IsBusyMainPage
    {
        
        DialogService dialogService;
        NavigationService navigationService;
        
        UserService userService;


        public string Nombre { get; set; }
        public string ApPaterno { get; set; }
        public string ApMaterno { get; set; }
        public string Sexo { get; set; }
        public string CorreoElectronico { get; set; }
        public string Contrasenia { get; set; }
        public string ConfirmarContrasenia { get; set; }
        public DateTime FechaNacimiento { get; set; }
        

        

        public MyAccountViewModel() {
            Debug.WriteLine("constructor de NewUserViewModel");
            

            dialogService = new DialogService();
            navigationService = new NavigationService();
            userService = new UserService();
            LoadData();
          

        }

        private void LoadData()
        {

            //FechaNacimiento = DateTime.Today;
            string f = Settings.UserFechaNacimiento+" 00:00:00.00";
            DateTime d = Convert.ToDateTime(f);
            FechaNacimiento = d;
            Nombre = Settings.UserNombre;
            ApPaterno = Settings.UserApPaterno;
            ApMaterno = Settings.UserApMaterno;
            Sexo = Settings.UserSexo;
            CorreoElectronico = Settings.UserCorreo;
            Contrasenia = Settings.UserContrasenia;
            ConfirmarContrasenia= Settings.UserContrasenia;


        }



        public ICommand SaveCommand
        {
            get { return new RelayCommand(Save); }
        }

        private async void Save()
        {


            if (Contrasenia == ConfirmarContrasenia)
            {
                IsBusy = true;
                User u = new User();
                u.Nombre = Nombre;
                u.ApMaterno = ApMaterno;
                u.ApPaterno = ApPaterno;
                u.Sexo = Settings.AuxSexo;
                String fn = FechaNacimiento.ToString("yyyy-MM-dd");
                u.FechaNacimiento = fn;
                u.Correo = CorreoElectronico;
                u.Contrasenia = Contrasenia;
                int status = await userService.UpdateUser(u,Settings.UserId);
                IsBusy = false;
                if (status == 1)
                    await dialogService.ShowMessage("Se guardaron los cambios safisfactoriamente", "Información");
                else if (status == 0)
                    await dialogService.ShowMessage("Faltan datos para editar el usuario ", "Error");
                

            }
            else
                await dialogService.ShowMessage("Las contraseñas no coinciden", "Error");

            
        }

        
    }
}
