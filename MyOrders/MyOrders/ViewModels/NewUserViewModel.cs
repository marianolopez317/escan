﻿using GalaSoft.MvvmLight.Command;
using MyOrders.Models;
using MyOrders.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Diagnostics;
using MyOrders.Helpers;

namespace MyOrders.ViewModels
{
    public class NewUserViewModel:IsBusyMainPage
    {

        
        DialogService dialogService;
        NavigationService navigationService;
        UserService userService;

        public string Nombre { get; set; }
        public string ApPaterno { get; set; }
        public string ApMaterno { get; set; }
        public string Sexo { get; set; }
        public string CorreoElectronico { get; set; }
        public string Contrasenia { get; set; }
        public string ConfirmarContrasenia { get; set; }
        public DateTime FechaNacimiento { get; set; }


        public NewUserViewModel() {
            userService = new UserService();
            dialogService = new DialogService();
            navigationService = new NavigationService();
            FechaNacimiento = DateTime.Today;
        }

        public ICommand SaveCommand
        {
            get { return new RelayCommand(Save); }
        }

        public ICommand HomeCommand
        {
            get { return new RelayCommand(Home); }
        }


        

        private async void Save()
        {
            if (Contrasenia == ConfirmarContrasenia) {
                IsBusy = true;
                User u = new User();
                u.Nombre = Nombre;
                u.ApMaterno = ApMaterno;
                u.ApPaterno = ApPaterno;
                u.Sexo = Settings.AuxSexo;
                String fn = FechaNacimiento.ToString("yyyy-MM-dd");
                u.FechaNacimiento = fn;
                u.Correo = CorreoElectronico;
                u.Contrasenia = Contrasenia;
                int status = await userService.CreateUser(u);
                IsBusy = false;
                if (status == 1)
                    await dialogService.ShowMessage("El Usuario se creo safisfactoriamente", "Información");
                else if (status == 0)
                    await dialogService.ShowMessage("Faltan datos para crear el usuario ", "Error");
                
                
                
            }   
            else
                await dialogService.ShowMessage("Las contraseñas no coinciden", "Error");

        }

        private void Home()
        {
            navigationService.SetMainPage("WelcomePage");
        }
    }
}
