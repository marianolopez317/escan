﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Diagnostics;
using MyOrders.Services;

namespace MyOrders.ViewModels
{
    public class WelcomeViewModel
    {

        NavigationService navigationService;

        //public NewUserViewModel NewUser { get; private set; }

        public LoginViewModel Login { get; private set; }

        public WelcomeViewModel() {
            navigationService = new NavigationService();
        }


        public ICommand StartLoginCommand
        {
            
            get { return new RelayCommand(StartLogin); }
        }


        public ICommand NewUserCommand
        {
        
            get { return new RelayCommand(NewUserM); }
        }

        private void NewUserM()
        {
            navigationService.SetMainPage("NewUserPage");
        }

        private void StartLogin()
        {            
            Login = new LoginViewModel();
            navigationService.SetMainPage("LoginPage");
        }
    }
}
