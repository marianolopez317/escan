﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyOrders.ViewModels
{
    public interface IImageLoader
    {
        Task<byte[]> LoadImageAsync(ImageSource source);
    }
}
