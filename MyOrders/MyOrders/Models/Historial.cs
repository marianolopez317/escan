﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class Historial
    {
        [JsonProperty("idRaza")]
        public int IdRaza { get; set; }
        [JsonProperty("fecha")]
        public String Fecha { get; set; }
    }
}
