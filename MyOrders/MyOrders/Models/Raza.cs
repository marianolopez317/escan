﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class Raza
    {
        [JsonProperty("idRaza")]
        public int IdUsuario { get; set; }
        [JsonProperty("nombre")]
        public string Nombre { get; set; }
        [JsonProperty("origen")]
        public string Origen { get; set; }
        [JsonProperty("uso")]
        public string Uso { get; set; }
        [JsonProperty("esperanzaVida")]
        public string EsperanzaVida { get; set; }
    }
}
