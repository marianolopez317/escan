﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyOrders.Models
{
    public class Account : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string nombre = "")
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nombre));
        }


        private String correoElectronico;
        public String CorreoElectronico
        {
            get { return correoElectronico; }
            set {
                correoElectronico = value;
                OnPropertyChanged();
            }
        }

        private String nombre;
        public String Nombre
        {
            get { return nombre; }
            set {
                nombre = value;
                OnPropertyChanged();
            }
        }

        private String apPaterno;
        public String ApPaterno
        {
            get { return apPaterno; }
            set {
                apPaterno = value;
                OnPropertyChanged();
            }
        }

        private String apMaterno;
        public String ApMaterno
        {
            get { return apMaterno; }
            set
            {
                apMaterno = value;
                OnPropertyChanged();
            }
        }

        private String sexo;
        public String Sexo
        {
            get { return sexo; }
            set
            {
                sexo = value;
                OnPropertyChanged();
            }
        }

        private DateTime fechaNacimiento;
        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
            set
            {
                fechaNacimiento = value;
                OnPropertyChanged();
            }
        }

        private String contrasenia;
        public String Contrasenia
        {
            get { return contrasenia; }
            set {
                contrasenia = value;
                OnPropertyChanged();
            }
        }

        private String confirmarContrasenia;
        public String ConfirmarContrasenia
        {
            get { return confirmarContrasenia; }
            set
            {
                confirmarContrasenia = value;
                OnPropertyChanged();
            }
        }
        /*
        private bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged();
            }
        }
        */
    }
}
