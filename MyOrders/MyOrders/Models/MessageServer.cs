﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class MessageServer
    {
        [JsonProperty("mensaje")]
        public String Mensaje { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
    }
}
