﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyOrders.Models
{
    public class Dog: INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string nombre = "")
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nombre));
        }

        private bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged();
            }
        }


        private bool isCuidado = false;
        public bool IsCuidado
        {
            get { return isCuidado; }
            set
            {
                isCuidado = value;
                OnPropertyChanged();
            }
        }


        private bool isAlimentacion = false;
        public bool IsAlimentacion
        {
            get { return isAlimentacion; }
            set
            {
                isAlimentacion = value;
                OnPropertyChanged();
            }
        }

        private bool isDatosCuriosos = false;
        public bool IsDatosCuriosos
        {
            get { return isDatosCuriosos; }
            set
            {
                isDatosCuriosos = value;
                OnPropertyChanged();
            }
        }



        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string Uso { get; set; }
        public string EsperanzaVida { get; set; }
        public string Imagen { get; set; }


        


    }
}
