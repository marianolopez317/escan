﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class DatoCurioso
    {
        [JsonProperty("idDatoCurioso")]
        public int IdDatoCurioso { get; set; }
        [JsonProperty("datoCurioso")]
        public string Dato { get; set; }
    }

}
