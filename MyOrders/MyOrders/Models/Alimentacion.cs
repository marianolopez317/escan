﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class Alimentacion
    {
        [JsonProperty("idAlimentacion")]
        public int IdAlimentacion { get; set; }
        [JsonProperty("tipo")]
        public string Tipo { get; set; }
        [JsonProperty("porcion")]
        public string Porcion { get; set; }
        [JsonProperty("frecuencia")]
        public string Frecuencia { get; set; }
        
    }
}
