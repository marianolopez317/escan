﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class User
    {
        [JsonProperty("idUsuario")]
        public int IdUsuario { get; set; }
        [JsonProperty("nombre")]
        public string Nombre { get; set; }
        [JsonProperty("apPaterno")]
        public string ApPaterno { get; set; }
        [JsonProperty("apMaterno")]
        public string ApMaterno { get; set; }
        [JsonProperty("correo")]
        public string Correo { get; set; }
        [JsonProperty("sexo")]
        public string Sexo { get; set; }
        [JsonProperty("fechaNacimiento")]
        public string FechaNacimiento { get; set; }
        [JsonProperty("contrasenia")]
        public string Contrasenia { get; set; }

    }
}
