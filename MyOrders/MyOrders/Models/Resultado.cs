﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class Resultado
    {
        [JsonProperty("resultado")]
        public String IdRaza { get; set; }
    }
}
