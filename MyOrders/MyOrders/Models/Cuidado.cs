﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class Cuidado
    {
        [JsonProperty("idCuidado")]
        public int IdCuidado { get; set; }
        [JsonProperty("pelo")]
        public string Pelo { get; set; }
        [JsonProperty("higiene")]
        public string Higiene { get; set; }
        [JsonProperty("vacuna")]
        public string Vacuna { get; set; }
        [JsonProperty("enfermedad")]
        public string Enfermedad { get; set; }
    }
}
