﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyOrders.Models
{
    public class ImagePack
    {
        [JsonProperty("idUsuario")]
        public String IdUsuario { get; set; }
        [JsonProperty("imagen")]
        public byte[] Imagen { get; set; }
    }
}
