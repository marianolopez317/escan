﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyOrders.Models
{
    public class Login : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string nombre = "")
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nombre));
        }


        private String correoElectronico;
        public String CorreoElectronico
        {
            get { return correoElectronico; }
            set { correoElectronico = value; }
        }       
        

        private String contrasenia;
        public String Contrasenia
        {
            get { return contrasenia; }
            set { contrasenia = value; }
        }


        private String mensajeInicioSesion;
        public String MensajeInicioSesion
        {
            get { return mensajeInicioSesion; }
            set {
                mensajeInicioSesion = value;
                OnPropertyChanged();
            }
        }


        private bool isBusy=false;
        public bool IsBusy
        {
            get { return isBusy; }
            set {
                isBusy = value;
                OnPropertyChanged();
            }
        }

        

        


    }
}
