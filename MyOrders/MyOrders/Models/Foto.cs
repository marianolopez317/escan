﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Xamarin.Forms;


namespace MyOrders.Models
{
    public class Foto : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string nombre = "")
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nombre));
        }

        private bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                OnPropertyChanged();
            }
        }


        private bool isTaken = false;
        public bool IsTaken
        {
            get { return isTaken; }
            set
            {
                isTaken = value;
                OnPropertyChanged();
            }
        }



        private ImageSource img;
        public ImageSource Img
        {
            get { return img; }
            set {
                img = value;
                OnPropertyChanged();
            }
        }


        

        private String base64;
        public String Base64
        {
            get { return base64; }
            set
            {
                base64 = value;
                //OnPropertyChanged();
            }
        }

        private byte[] b;
        public byte[] B
        {
            get { return b; }
            set
            {
                b = value;
                //OnPropertyChanged();
            }
        }









    }
}
